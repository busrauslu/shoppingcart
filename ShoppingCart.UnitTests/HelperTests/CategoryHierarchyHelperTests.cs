﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using ShoppingCart.Helpers;
using ShoppingCart.Model;
using Xunit;

namespace ShoppingCart.UnitTests.HelperTests
{
    public class CategoryHierarchyHelperTests
    {
        [Fact]
        public void Should_return_empty_list_when_title_is_null()
        {
            var result = CategoryHierarchyHelper.GetCategoryHierarchy(new Category(null), new List<string>());
            result.Count.Should().Be(0);
        }
        [Fact]
        public void Should_return_empty_list_when_title_is_empty()
        {
            var result = CategoryHierarchyHelper.GetCategoryHierarchy(new Category(""), new List<string>());
            result.Count.Should().Be(0);
        }

        [Fact]
        public void Should_get_category_hierarchy_when_one_category_exist()
        {
            var result = CategoryHierarchyHelper.GetCategoryHierarchy(new Category("title"), new List<string>());
            result.Count.Should().Be(1);
            result.First().Should().Be("title");
        }

        [Fact]
        public void Should_get_category_hierarchy_with_parents()
        {
            var categoryWithHierarchy = new Category("title1", new Category("title2", new Category("title3", new Category("title4", new Category("title5")))));
            var result = CategoryHierarchyHelper.GetCategoryHierarchy(categoryWithHierarchy, new List<string>());
            result.Count.Should().Be(5);
            result.First().Should().Be("title1");
            result.Last().Should().Be("title5");
        }

        [Fact]
        public void Should_not_get_category_hierarchy_without_title()
        {
            var categoryWithHierarchy = new Category("title1", new Category("title2", new Category("title3", new Category("title4", new Category("")))));
            var result = CategoryHierarchyHelper.GetCategoryHierarchy(categoryWithHierarchy, new List<string>());
            result.Count.Should().Be(4);
            result.First().Should().Be("title1");
            result.Last().Should().Be("title4");
        }
    }
}
