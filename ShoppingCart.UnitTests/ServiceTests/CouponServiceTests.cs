﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using ShoppingCart.Model;
using ShoppingCart.Service;
using Xunit;

namespace ShoppingCart.UnitTests.ServiceTests
{
    public class CouponServiceTests
    {
        [Fact]
        public void Should_get_empty_coupon_list()
        {
            var couponService = new CouponService(new List<Coupon>());
            couponService.GetAllCoupons().Count.Should().Be(0);
        }

        [Fact]
        public void Should_get_campaign_list()
        {
            var campaignList = new List<Coupon>
            {
                new Coupon(15, 1, DiscountType.Amount),
            };
            var campaignsService = new CouponService(campaignList);
            var result = campaignsService.GetAllCoupons();

            result.Count.Should().Be(1);
            result.First().DiscountType.Should().Be(DiscountType.Amount);
        }
    }
}
