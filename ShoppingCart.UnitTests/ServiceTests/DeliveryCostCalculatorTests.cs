﻿using FluentAssertions;
using Moq;
using ShoppingCart.Interface;
using ShoppingCart.Service;
using Xunit;

namespace ShoppingCart.UnitTests.ServiceTests
{ public class DeliveryCostCalculatorTests
    {
        private IDeliveryCostCalculator _deliveryCostCalculator;
        private readonly Mock<IShoppingCartService> _mockShoppingCartService;
        public DeliveryCostCalculatorTests()
        {
            _mockShoppingCartService = new Mock<IShoppingCartService>();
        }

        [Theory]
        [InlineData(0, 0, 15, 10, 2.99)]
        [InlineData(1, 1, 15, 10, 27.99)]
        [InlineData(10, 15, 1, 1, 27.99)]
        [InlineData(5, 5, 5, 5, 52.99)]
        public void Should_calculate_delivery_cost(int numberOfDeliveries, int numberOfProduct, decimal costPerDelivery, decimal costPerProduct, decimal result)
        {
            _deliveryCostCalculator = new DeliveryCostCalculator(costPerDelivery, costPerProduct);
            _mockShoppingCartService.Setup(m => m.GetNumberOfDeliveries()).Returns(numberOfDeliveries);
            _mockShoppingCartService.Setup(m => m.GetNumberOfProduct()).Returns(numberOfProduct);

           _deliveryCostCalculator.CalculateFor(_mockShoppingCartService.Object).Should().Be(result);
        }
    }
}
