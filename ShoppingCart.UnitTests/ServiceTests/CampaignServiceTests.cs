﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using ShoppingCart.Model;
using ShoppingCart.Service;
using Xunit;

namespace ShoppingCart.UnitTests.ServiceTests
{
    public class CampaignServiceTests
    {
        [Fact]
        public void Should_get_empty_campaign_list()
        {
            var campaignsService = new CampaignsService(new List<Campaign>());
            campaignsService.GetAllCampaigns().Count.Should().Be(0);
        }

        [Fact]
        public void Should_get_campaign_list()
        {
            var campaignList = new List<Campaign>
            {
                new Campaign(new Category("title"), 15, 1, DiscountType.Amount),
                new Campaign(new Category("title2"), 10, 2, DiscountType.Rate)
            };
            var campaignsService = new CampaignsService(campaignList);
            var result = campaignsService.GetAllCampaigns();

            result.Count.Should().Be(2);
            result.First().DiscountType.Should().Be(DiscountType.Amount);
            result.Last().DiscountType.Should().Be(DiscountType.Rate);
        }
    }
}
