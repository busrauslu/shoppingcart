﻿using System.Collections.Generic;
using FluentAssertions;
using Moq;
using ShoppingCart.Interface;
using ShoppingCart.Model;
using ShoppingCart.Service;
using Xunit;

namespace ShoppingCart.UnitTests.ServiceTests
{
    public class ShoppingCartServiceTests
    {
        private ShoppingCartService _shoppingCartService;
        private readonly Mock<ICampaignsService> _mockCampaignService;
        private readonly Mock<ICouponService> _mockCouponService;
        private readonly Mock<IDeliveryCostCalculator> _mockDeliveryCostCalculator;

        public ShoppingCartServiceTests()
        {
            _mockCampaignService = new Mock<ICampaignsService>();
            _mockCouponService = new Mock<ICouponService>();
            _mockDeliveryCostCalculator = new Mock<IDeliveryCostCalculator>();
        }

        [Fact]
        public void Should_not_add_product_when_quantity_is_zero()
        {
            _shoppingCartService = new ShoppingCartService(null, null, null);
            _shoppingCartService.AddProductToCart(new Product("title", 10, new Category("categoryTitle")), 0 );
            _shoppingCartService.GetNumberOfProduct().Should().Be(0);
        }

        [Fact]
        public void Should_not_add_product_when_product_is_null()
        {
            _shoppingCartService = new ShoppingCartService(null, null, null);
            _shoppingCartService.AddProductToCart(null, 1);
            _shoppingCartService.GetNumberOfProduct().Should().Be(0);
        }

        [Fact]
        public void Should_not_add_product_when_product_price_is_zero()
        {
            _shoppingCartService = new ShoppingCartService(null, null, null);
            _shoppingCartService.AddProductToCart(new Product("title", 0, new Category("categoryTitle")), 1);
            _shoppingCartService.GetNumberOfProduct().Should().Be(0);
        }

        [Fact]
        public void Should_add_product_to_cart()
        {
            _shoppingCartService = new ShoppingCartService(null, null, null);
            _shoppingCartService.AddProductToCart(new Product("title", 10, new Category("categoryTitle")), 1);
            _shoppingCartService.AddProductToCart(new Product("title2", 20, new Category("categoryTitle2")), 2);
            _shoppingCartService.GetNumberOfProduct().Should().Be(2);
        }

        [Fact]
        public void Should_not_apply_campaign_when_product_category_hierarchy_has_not_any_campaigns()
        {
            _mockCampaignService.Setup(x => x.GetAllCampaigns()).Returns(new List<Campaign>
            {
                new Campaign(new Category("notExistCategory"), 10, 1, DiscountType.Amount)
            });

            _shoppingCartService = new ShoppingCartService(_mockCampaignService.Object, null, null);
            _shoppingCartService.AddProductToCart(new Product("title", 100, new Category("categoryTitle")), 1);

            _shoppingCartService.ApplyCampaigns();

            _shoppingCartService.GetTotalCampaignAmount().Should().Be(0);
        }


        [Fact]
        public void Should_not_apply_campaign_when_campaigns_amount_greater_then_cart_products_total_amount()
        {
            _mockCampaignService.Setup(x => x.GetAllCampaigns()).Returns(new List<Campaign>
            {
                new Campaign(new Category("categoryTitle"), 1000, 1, DiscountType.Amount)
            });

            _shoppingCartService = new ShoppingCartService(_mockCampaignService.Object, null, null);
            _shoppingCartService.AddProductToCart(new Product("title", 100, new Category("categoryTitle")), 1);

            _shoppingCartService.ApplyCampaigns();

            _shoppingCartService.GetTotalCampaignAmount().Should().Be(0);
        }


        [Theory]
        [InlineData(2, 1)]
        [InlineData(100, 99)]
        [InlineData(1000000, 1)]
        public void Should_not_apply_campaign_when_cart_product_quantity_is_not_valid_for_campaigns(int campaignQuantity, int cartQuantity)
        {
            _mockCampaignService.Setup(x => x.GetAllCampaigns()).Returns(new List<Campaign>
            {
                new Campaign(new Category("categoryTitle"), 10, campaignQuantity, DiscountType.Amount)
            });

            _shoppingCartService = new ShoppingCartService(_mockCampaignService.Object, null, null);
            _shoppingCartService.AddProductToCart(new Product("title", 100, new Category("categoryTitle")), cartQuantity);

            _shoppingCartService.ApplyCampaigns();

            _shoppingCartService.GetTotalCampaignAmount().Should().Be(0);
        }

        [Fact]
        public void Should_apply_amount_type_campaign()
        {
            _mockCampaignService.Setup(x => x.GetAllCampaigns()).Returns(new List<Campaign>
            {
                new Campaign(new Category("categoryTitle"), 10, 2, DiscountType.Amount)
            });

            _shoppingCartService = new ShoppingCartService(_mockCampaignService.Object, null, null);
            _shoppingCartService.AddProductToCart(new Product("title", 100, new Category("categoryTitle")), 2);

            _shoppingCartService.ApplyCampaigns();

            _shoppingCartService.GetTotalCampaignAmount().Should().Be(10);
        }

        [Fact]
        public void Should_apply_rate_type_campaign()
        {
            _mockCampaignService.Setup(x => x.GetAllCampaigns()).Returns(new List<Campaign>
            {
                new Campaign(new Category("categoryTitle"), 20, 2, DiscountType.Rate)
            });

            _shoppingCartService = new ShoppingCartService(_mockCampaignService.Object, null, null);
            _shoppingCartService.AddProductToCart(new Product("title", 100, new Category("categoryTitle")), 2);

            _shoppingCartService.ApplyCampaigns();

            _shoppingCartService.GetTotalCampaignAmount().Should().Be(40);
        }


        [Fact]
        public void Should_apply_more_than_one_campaigns_for_one_product()
        {
            _mockCampaignService.Setup(x => x.GetAllCampaigns()).Returns(new List<Campaign>
            {
                new Campaign(new Category("categoryTitle"), 20, 2, DiscountType.Rate), //400
                new Campaign(new Category("categoryTitle"), 20, 2, DiscountType.Amount), //380   
                new Campaign(new Category("parentCategoryTitle"), 20, 2, DiscountType.Amount), //360
            });

            _shoppingCartService = new ShoppingCartService(_mockCampaignService.Object, null, null);
            _shoppingCartService.AddProductToCart(new Product("title", 100, new Category("categoryTitle", new Category("parentCategoryTitle"))), 5);

            _shoppingCartService.ApplyCampaigns();

            _shoppingCartService.GetTotalCampaignAmount().Should().Be(140);
        }

        [Fact]
        public void Should_apply_more_than_one_campaigns_for_more_than_one_products()
        {
            _mockCampaignService.Setup(x => x.GetAllCampaigns()).Returns(new List<Campaign>
            {
                new Campaign(new Category("categoryTitle"), 20, 2, DiscountType.Rate), 
                new Campaign(new Category("categoryTitle"), 20, 2, DiscountType.Amount),
                new Campaign(new Category("parentCategoryTitle"), 20, 2, DiscountType.Amount),
                new Campaign(new Category("parentParentCategoryTitle2"), 20, 2, DiscountType.Amount),
                new Campaign(new Category("categoryTitle2"), 25, 2, DiscountType.Rate), 
            });
            _shoppingCartService = new ShoppingCartService(_mockCampaignService.Object, null, null);
            _shoppingCartService.AddProductToCart(new Product("title", 100, new Category("categoryTitle", new Category("parentCategoryTitle"))), 5);
            _shoppingCartService.AddProductToCart(new Product("title2", 250, new Category("categoryTitle2", new Category("parentCategoryTitle2", new Category("parentParentCategoryTitle2")))), 4);

            _shoppingCartService.ApplyCampaigns();

            _shoppingCartService.GetTotalCampaignAmount().Should().Be(410);
        }

        [Theory]
        [InlineData(10, 1, 10)]
        [InlineData(500, 500, 500)]
        [InlineData(501, 501, 0)]
        [InlineData(20, 500, 20)]
        [InlineData(20, 501, 0)]
        public void Should_get_applied_coupon_results(int couponRate, int quantity, int result)
        {
            _mockCampaignService.Setup(x => x.GetAllCampaigns()).Returns(new List<Campaign>
            {
                new Campaign(new Category("categoryTitle"), 20, 2, DiscountType.Rate),
                new Campaign(new Category("categoryTitle"), 20, 2, DiscountType.Amount),
                new Campaign(new Category("parentCategoryTitle"), 20, 2, DiscountType.Amount),
            });

            _mockCouponService.Setup(x => x.GetAllCoupons()).Returns(new List<Coupon>
            {
                new Coupon(couponRate, quantity, DiscountType.Amount)
            });
            _shoppingCartService = new ShoppingCartService(_mockCampaignService.Object, _mockCouponService.Object, null);

            _shoppingCartService.AddProductToCart(new Product("title", 100, new Category("categoryTitle", new Category("parentCategoryTitle"))), 1);
            _shoppingCartService.AddProductToCart(new Product("title2", 200, new Category("categoryTitle2")), 2);

            _shoppingCartService.ApplyCoupons();

            _shoppingCartService.GetTotalCouponAmount().Should().Be(result);
        }

        [Theory]
        [InlineData(10, 1, 10, 2670)]
        [InlineData(500, 500, 20, 1880)]
        [InlineData(100, 20, 50, 1380)]
        public void Should_get_cart_amount(int couponRate, int quantity, int campaignDiscountRate, int result)
        {
            _mockCampaignService.Setup(x => x.GetAllCampaigns()).Returns(new List<Campaign>
            {
                new Campaign(new Category("categoryTitle"), campaignDiscountRate, 2, DiscountType.Rate),
                new Campaign(new Category("parentCategoryTitle"), 20, 2, DiscountType.Amount),
            });

            _mockCouponService.Setup(x => x.GetAllCoupons()).Returns(new List<Coupon>
            {
                new Coupon(couponRate, quantity, DiscountType.Amount)
            });
            _shoppingCartService = new ShoppingCartService(_mockCampaignService.Object, _mockCouponService.Object, null);

            _shoppingCartService.AddProductToCart(new Product("title", 1000, new Category("categoryTitle", new Category("parentCategoryTitle"))), 3);

            _shoppingCartService.ApplyCampaigns();
            _shoppingCartService.ApplyCoupons();

            _shoppingCartService.TotalBasketAmountAfterCampaignsAndCoupons().Should().Be(result);
        }
    }
}
