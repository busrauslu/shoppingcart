# Shopping Cart Example
Implemented shopping cart e commerce project

## Getting Started
### Prerequisites
[.Net Core SDK 3.1]
[Visual Studio 2019 Preview]

### Installing
an example of getting some data out of the system

cd /ShoppingCart
Click ShoppingCart.sln 

for the shortcut:
cd /ShoppingCart
```
cmd.exe /c open-solution.bat
```

for running: 
cd /ShoppingCart
```
dotnet run
```


### Running the tests

cd /ShoppingCart.UnitTests
```
dotnet clean
dotnet restore
dotnet build
dotnet test --no-build
```

for the shortcut:
cd /ShoppingCart
```
cmd.exe /c build.bat
```


## Built With

* [.Net Core 3.1](https://dotnet.microsoft.com/download/dotnet-core/3.1) - The framework used
* [Moq](https://www.nuget.org/packages/Moq/) - Using with tests
* [Xunit](https://www.nuget.org/packages/xunit/) - Using with tests
* [FluentAssertions] (https://www.nuget.org/packages/FluentAssertions/) - Using with tests
* [xunit.runner.visualstudio] (https://www.nuget.org/packages/xunit.runner.visualstudio/) - Using with tests

## Authors

* **Busra Uslu** 
