﻿namespace ShoppingCart
{
    using Model;
    using Service;
    using System.Collections.Generic;
    public class Program
    {
        static void Main(string[] args)
        {
            var category1 = new Category("category1");
            var category2 = new Category("category2", category1);
            var category3 = new Category("category3", category2);
            var category4 = new Category("category4");

            var product1 = new Product("product1", 100.0M, category3);
            var product2 = new Product("product2", 200.0M, category1);

            var activeCampaignList = new List<Campaign>
            {
                new Campaign(category1, 10, 2, DiscountType.Rate),
                new Campaign(category1, 20, 1, DiscountType.Amount),
                new Campaign(category3, 50, 2, DiscountType.Rate),
                new Campaign(category4, 50, 1, DiscountType.Rate)
            };

            var activeCoupons = new List<Coupon>
            {
                new Coupon(10,  2, DiscountType.Amount)
            };
           
            var shoppingCartService = new ShoppingCartService(
                new CampaignsService(activeCampaignList),
                new CouponService(activeCoupons),
                new DeliveryCostCalculator(5, 5));

            shoppingCartService.AddProductToCart(product1, 2);
            shoppingCartService.AddProductToCart(product2, 3);


            shoppingCartService.ApplyCampaigns();
            shoppingCartService.ApplyCoupons();

            shoppingCartService.Print();
        }
    }
}
