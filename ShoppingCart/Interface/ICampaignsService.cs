﻿namespace ShoppingCart.Interface
{
    using System.Collections.Generic;
    using Model;

    public interface ICampaignsService
    {
        List<Campaign> GetAllCampaigns();
    }
}
