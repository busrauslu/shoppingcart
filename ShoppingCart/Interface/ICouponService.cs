﻿namespace ShoppingCart.Interface
{
    using System.Collections.Generic;
    using Model;

    public interface ICouponService
    {
        List<Coupon> GetAllCoupons();
    }
}
