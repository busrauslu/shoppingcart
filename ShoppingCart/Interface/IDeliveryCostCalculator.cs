﻿namespace ShoppingCart.Interface
{
    public interface IDeliveryCostCalculator
    {
        decimal CalculateFor(IShoppingCartService shoppingCartService);
    }
}
