﻿namespace ShoppingCart.Interface
{
    public interface IShoppingCartService
    {
        int GetNumberOfProduct();
        int GetNumberOfDeliveries();
    }
}
