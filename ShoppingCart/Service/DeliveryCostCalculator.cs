﻿namespace ShoppingCart.Service
{
    using Interface;
    public class DeliveryCostCalculator : IDeliveryCostCalculator
    {
        public decimal CostPerDelivery { get; }
        public decimal CostPerProduct { get; }
        public decimal FixedCost { get; }
        public DeliveryCostCalculator(decimal costPerDelivery, decimal costPerProduct, decimal fixedCost = 2.99M)
        {
            CostPerDelivery = costPerDelivery;
            CostPerProduct = costPerProduct;
            FixedCost = fixedCost;
        }
        public decimal CalculateFor(IShoppingCartService shoppingCartService)
        {
            int numberOfProducts = shoppingCartService.GetNumberOfProduct();
            int numberOfDeliveries = shoppingCartService.GetNumberOfDeliveries();
            return (CostPerDelivery * numberOfDeliveries) + (CostPerProduct * numberOfProducts) + FixedCost;
        }
    }
}
