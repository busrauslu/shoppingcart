﻿namespace ShoppingCart.Service
{
    using System.Collections.Generic;
    using Interface;
    using Model;

    public class CampaignsService : ICampaignsService
    {
        private readonly List<Campaign> _activeCampaigns;
        public CampaignsService(List<Campaign> activeCampaigns)
        {
            _activeCampaigns = new List<Campaign>();
            _activeCampaigns = activeCampaigns;
        }

        public List<Campaign> GetAllCampaigns()
        {
            return _activeCampaigns;
        }
    }
}
