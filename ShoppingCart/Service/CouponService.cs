﻿namespace ShoppingCart.Service
{
    using System.Collections.Generic;
    using Model;
    using Interface;

    public class CouponService : ICouponService
    {
        private readonly List<Coupon> _activeCoupons;
        public CouponService(List<Coupon> activeCoupons)
        {
            _activeCoupons = new List<Coupon>();
            _activeCoupons = activeCoupons;
        }

        public List<Coupon> GetAllCoupons()
        {
            return _activeCoupons;
        }
    }
}
