﻿using ShoppingCart.Helpers;

namespace ShoppingCart.Service
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Model;
    using Interface;

    public class ShoppingCartService : IShoppingCartService
    {
        private readonly ICampaignsService _campaignsService;
        private readonly ICouponService _couponService;
        private readonly IDeliveryCostCalculator _deliveryCostCalculator;
        private readonly List<CartProduct> _cartProductList;
        private decimal _totalCampaignAmount;
        private decimal _totalCouponAmount;
        private int _numberOfDeliveries;

        public ShoppingCartService(
            ICampaignsService campaignsService, 
            ICouponService couponService, 
            IDeliveryCostCalculator deliveryCostCalculator)
        {
            _campaignsService = campaignsService;
            _couponService = couponService;
            _deliveryCostCalculator = deliveryCostCalculator;
            _cartProductList = new List<CartProduct>();
        }

        public void AddProductToCart(Product product, int quantity)
        {
            if (IsValidAddingProductToCart(product, quantity))
            {
                _cartProductList.Add(new CartProduct(product, quantity));
            }
            else
            {
                Console.WriteLine("Product cannot be add to basket");
            }

        }

        public int GetNumberOfProduct()
        {
            return _cartProductList.Count;
        }

        public int GetNumberOfDeliveries()
        {
            return _numberOfDeliveries;
        }

        public void ApplyCampaigns()
        {
            foreach (var cartProduct in _cartProductList)
            {
                var categoryHierarchy = CategoryHierarchyHelper.GetCategoryHierarchy(cartProduct.Product.Category, new List<string>());
                _numberOfDeliveries += categoryHierarchy.Count;
                var validCampaignsForProduct = GetCampaignsForProduct(categoryHierarchy, cartProduct);
                _totalCampaignAmount += GetCampaignDiscount(cartProduct, validCampaignsForProduct);
            }
        }

        public void ApplyCoupons()
        {
            var coupons = _couponService.GetAllCoupons();

            foreach (var coupon in coupons)
            {
                if (coupon.Quantity <= TotalBasketAmountAfterCampaigns())
                {
                    _totalCouponAmount += coupon.DiscountRate;
                }
            }
        }

        public decimal GetTotalCouponAmount()
        {
            return _totalCouponAmount;
        }

        public decimal GetTotalCampaignAmount()
        {
            return _totalCampaignAmount;
        }
        public decimal TotalBasketAmountAfterCampaignsAndCoupons()
        {
            return GetTotalAmountWithoutCampaignsAndCoupons() - GetTotalCampaignAmount() - GetTotalCouponAmount();
        }

        public void Print()
        {
            Console.WriteLine($"Total Amount Without Campaigns and Coupons {GetTotalAmountWithoutCampaignsAndCoupons()}");
            Console.WriteLine($"Total Campaign Discount {GetTotalCampaignAmount()}");
            Console.WriteLine($"Total Coupon Discount {GetTotalCouponAmount()}");
            Console.WriteLine($"Total Amount With Campaigns and Coupons {TotalBasketAmountAfterCampaignsAndCoupons()}");
            Console.WriteLine($"Total Delivery Cost {_deliveryCostCalculator.CalculateFor(this)}");
        }

        private decimal TotalAmountOfProducts(CartProduct cartProduct)
        {
            return cartProduct.Product.Price * cartProduct.Quantity;
        }

        private decimal GetCampaignDiscount(CartProduct cartProduct, List<Campaign> campaigns)
        {
            decimal productPrice = TotalAmountOfProducts(cartProduct);
            decimal totalDiscount = 0;
            foreach (var campaign in campaigns)
            {
                switch (campaign.DiscountType)
                {
                    case DiscountType.Rate:
                    {
                        var campaignAmount = productPrice * (campaign.DiscountRate / 100);
                        totalDiscount += campaignAmount;
                        break;
                    }
                    case DiscountType.Amount:
                    {
                        if (productPrice >= campaign.DiscountRate)
                        {
                            totalDiscount += campaign.DiscountRate;
                        }
                       
                        break;
                    }
                }
            }
            return totalDiscount;
        }

        private List<Campaign> GetCampaignsForProduct(List<string> categoryList, CartProduct cartProduct)
        {
            var allCampaignList = _campaignsService.GetAllCampaigns();
            var result = new List<Campaign>();

            foreach (var category in categoryList)
            {
                var campaigns = allCampaignList.Where(z=>z.Category.Title == category).ToList();

                if (campaigns.Any())
                {
                    foreach (var campaign in campaigns)
                    {
                        if (cartProduct.Quantity >= campaign.Quantity)
                        {
                            result.Add(campaign);
                        }
                    }
                }
            }
            return result;
        }

        private decimal TotalBasketAmountAfterCampaigns()
        {
            return GetTotalAmountWithoutCampaignsAndCoupons() - GetTotalCampaignAmount();
        }
        
        private decimal GetTotalAmountWithoutCampaignsAndCoupons()
        {
            decimal amount = 0;
            if (_cartProductList.Count > 0)
            {
                _cartProductList.ForEach(x =>
                {
                    amount += x.Quantity * x.Product.Price;
                });
            }

            return amount;
        }

        private bool IsValidAddingProductToCart(Product product, int quantity)
        {
            return quantity != 0 && product != null && product?.Price != 0;
        }
    }
}
