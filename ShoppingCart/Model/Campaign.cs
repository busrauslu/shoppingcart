﻿namespace ShoppingCart.Model
{
    public class Campaign  : Discount
    {
        public Campaign(Category category, decimal discountRate, int quantity, DiscountType discountType)
            : base(discountType, discountRate, quantity)
        {
            Category = category;

        }
        public Category Category { get; set; }

    }
}
