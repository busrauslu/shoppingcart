﻿namespace ShoppingCart.Model
{
    public class Coupon : Discount
    {
        public Coupon(decimal discountRate, int quantity, DiscountType discountType)
            : base(discountType, discountRate, quantity)
        {
        }
    }
}
