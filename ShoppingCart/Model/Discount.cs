﻿namespace ShoppingCart.Model
{
    public abstract class Discount
    {
        protected Discount(DiscountType discountType,  decimal discountRate, int quantity)
        { 
            DiscountType = discountType;
            Quantity = quantity;
            DiscountRate = discountRate;
        }

        public DiscountType DiscountType { get;} 
        public int Quantity { get; }
        public decimal DiscountRate { get; }
    }
}