﻿namespace ShoppingCart.Model
{
    public class CartProduct
    {
        public CartProduct(Product product, int quantity)
        {
            Product = product;
            Quantity = quantity;
        }
        public Product Product { get; set; }
        public int Quantity { get; set; }
    }
}
