﻿namespace ShoppingCart.Model
{
    public class Product
    {
        public Product(string title, decimal price, Category category)
        {
            Title = title;
            Price = price;
            Category = category;
        }

        public string Title { get; set; }
        public decimal Price { get; set; }
        public Category Category { get; set; }
    }
}