﻿using System.Collections.Generic;
using ShoppingCart.Model;

namespace ShoppingCart.Helpers
{
    public static class CategoryHierarchyHelper
    {
        public static List<string> GetCategoryHierarchy(Category category, List<string> categoryList)
        {
            if (!string.IsNullOrEmpty(category.Title))
            {
                categoryList.Add(category.Title);
            }
            if (category.ParentCategory != null)
            {
                GetCategoryHierarchy(category.ParentCategory, categoryList);
            }
            return categoryList;
        }
    }
}
